# Codewars 2016

This project is generated with [yo angular generator](https://github.com/yeoman/generator-angular)
version 0.11.1. Check the page in order to install the initial dependencies for the angular project.
After that run `npm install` and `bower i` to install the project dependencies.

## Build & development

Run `grunt` for building and `grunt serve` for preview.

## Testing

Running `grunt test` will run the unit tests with karma.


## Backend ##
### 1. Details ###

* the backend runs like a server application, listening on port 8081
* it exposes a REST webservice so that other applications can access it (e.g the frontend)

### 2. Prerequisites ###
You need the following installed and available in your PATH environment variable:

* Java 8 (http://www.oracle.com/technetwork/java/javase/downloads/index.html)
* Apache Maven 3.2.5 or greater (http://maven.apache.org/download.cgi)

### 3. Build and Run ###
Run the following commands in /backend folder:
```
#!shell
# Building
mvn clean package -Dmaven.test.skip=true

# Running
java -jar target/dependency/jetty-runner.jar --port 8081 target/*.war

# or you can use a single command
mvn jetty:run
```

For testing the backend functionality navigate to http://localhost:8081/ and you will see all the possible REST API calls.