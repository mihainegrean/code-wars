'use strict';

angular.module('codewars.services')
    .service('userService', [
        '$http',
        '$httpParamSerializer',
        '$q',
        '$timeout',
        'notificationService',
        'Base64',
        '$rootScope',
        '$cookies',
        'REST_BASE',
        function ($http,$httpParamSerializer, $q, $timeout, notificationService, Base64, $rootScope, $cookies, REST_BASE) {

            var self = this;

            self.user = {};

            self.setCredentials = function (username, password) {
                var authdata = Base64.encode(username + ':' + password);

                $rootScope.globals = {
                    currentUser: {
                        username: username,
                        authdata: authdata
                    }
                };

                $http.defaults.headers.common.Authorization = 'Basic ' + authdata;  //jshint ignore:line
                $cookies.putObject('globals', $rootScope.globals);
            };

            self.addAdvancedData = function (advancedDetails) {
                $rootScope.globals.currentUser.advancedDetails = advancedDetails;
                $cookies.putObject('globals', $rootScope.globals);
            };

            self.login = function (user) {
                var defer = $q.defer();
                self.setCredentials(user.username, user.password);
                $http({
                    url: REST_BASE.USERS + '/login',
                    method: 'POST',
                    data: $httpParamSerializer(user),
                    headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
                }).then(function (result) {
                        self.user = result.data;
                        self.addAdvancedData(self.user);
                        defer.resolve(result.data);
                    },
                    function (error) {
                        self.logout();
                        defer.reject(error);
                    });

                return defer.promise;
            };

            self.logout = function () {
                $rootScope.globals = {};
                $cookies.remove('globals');
                $http.defaults.headers.common.Authorization = 'Basic ';
            };

        }]);
